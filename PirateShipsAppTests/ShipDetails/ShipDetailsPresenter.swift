//
//  ShipDetailsPresenter.swift
//  PirateShipsAppTests
//
//  Created by Victor Magalhaes on 18/03/19.
//  Copyright © 2019 Victor. All rights reserved.
//

import XCTest

@testable import PirateShipsApp

private class ShipDetailsPresenterTests: XCTestCase {

    private var view: ShipDetailsViewSpy!
    private var router: ShipDetailsRouteringSpy!
    private var sut_presenter: ShipDetailsPresenter!
    
    private func setup(ship: Ship) {
        view = ShipDetailsViewSpy()
        router = ShipDetailsRouteringSpy()
        sut_presenter = ShipDetailsPresenter(router: router, ship: ship)
        sut_presenter.attachView(view)
    }

    func test_when_view_did_load() {
        let ship = Ship(id: 123, title: "title value", imageUrl: "image value", description: "description value", type: .yo, price: 10)
        setup(ship: ship)
        
        XCTAssert(view.setImageCalled == true)
        XCTAssert(view.imagePassed == "image value")
        
        XCTAssert(view.setTitleCalled == true)
        XCTAssert(view.titlePassed == "title value")
        
        XCTAssert(view.setDescriptionCalled == true)
        XCTAssert(view.descriptionPassed == "description value")
        
        XCTAssert(view.setPriceCalled == true)
        XCTAssert(view.pricePassed == "$10.00")
    }
    
    func test_when_view_did_load_with_empty_parameters() {
        let ship = Ship(output: ShipOutput(id: 0, title: nil, image: nil, detail: nil, type: nil, price: nil))
        setup(ship: ship)
        
        XCTAssert(view.setImageCalled == true)
        XCTAssert(view.imagePassed == "https://ichef.bbci.co.uk/images/ic/640x360/p06gsqm7.jpg")
        
        XCTAssert(view.setTitleCalled == true)
        XCTAssert(view.titlePassed == "Unknown")
        
        XCTAssert(view.setDescriptionCalled == true)
        XCTAssert(view.descriptionPassed == "Unknown")
        
        XCTAssert(view.setPriceCalled == true)
        XCTAssert(view.pricePassed == "$xxx.xx")
    }
    
    func test_when_pirate_greeting_yo_triggered() {
        let ship = Ship(id: 123, title: "title value", imageUrl: "image value", description: "description value", type: .yo, price: 10)
        setup(ship: ship)
        sut_presenter.pirateGreetingTriggered()
        
        XCTAssert(view.showPirateGreetingCalled == true)
        XCTAssert(view.pirateGreetingPassed == "Yo ho hooo!")
    }
    
    func test_when_pirate_greeting_ar_triggered() {
        let ship = Ship(id: 123, title: "title value", imageUrl: "image value", description: "description value", type: .ar, price: 10)
        setup(ship: ship)
        sut_presenter.pirateGreetingTriggered()
        
        XCTAssert(view.showPirateGreetingCalled == true)
        XCTAssert(view.pirateGreetingPassed == "Arrr!")
    }
    
    func test_when_pirate_greeting_ay_triggered() {
        let ship = Ship(id: 123, title: "title value", imageUrl: "image value", description: "description value", type: .ay, price: 10)
        setup(ship: ship)
        sut_presenter.pirateGreetingTriggered()
        
        XCTAssert(view.showPirateGreetingCalled == true)
        XCTAssert(view.pirateGreetingPassed == "Aye Aye!")
    }
    
    func test_when_pirate_greeting_ah_triggered() {
        let ship = Ship(id: 123, title: "title value", imageUrl: "image value", description: "description value", type: .ah, price: 10)
        setup(ship: ship)
        sut_presenter.pirateGreetingTriggered()
        
        XCTAssert(view.showPirateGreetingCalled == true)
        XCTAssert(view.pirateGreetingPassed == "Ahoi!")
    }
    
    func test_when_close_button_triggered() {
        let ship = Ship(id: 123, title: "title value", imageUrl: "image value", description: "description value", type: .ah, price: 10)
        setup(ship: ship)
        sut_presenter.closeButtonTriggered()
        
        XCTAssert(router.dismissCalled == true)
    }
    
}

final class ShipDetailsViewSpy: ShipDetailsView {
    
    var setImageCalled: Bool?
    var imagePassed: String?
    var setTitleCalled: Bool?
    var titlePassed: String?
    var setDescriptionCalled: Bool?
    var descriptionPassed: String?
    var setPriceCalled: Bool?
    var pricePassed: String?
    var showPirateGreetingCalled: Bool?
    var pirateGreetingPassed: String?
    
    func setTitle(_ text: String) {
        setTitleCalled = true
        titlePassed = text
    }
    
    func setDescription(_ text: String) {
        setDescriptionCalled = true
        descriptionPassed = text
    }
    
    func setImage(_ text: String) {
        imagePassed = text
        setImageCalled = true
    }
    
    func setPrice(_ text: String) {
        setPriceCalled = true
        pricePassed = text
    }
    
    func showPirateGreeting(_ text: String) {
        pirateGreetingPassed = text
        showPirateGreetingCalled = true
    }
}

final class ShipDetailsRouteringSpy: ShipDetailsRoutering {
    var dismissCalled: Bool?
    
    func dismiss() {
        dismissCalled = true
    }
}
