# PirateShips
An app listing a list of pirate ships and showing them details.

![Alt text](https://media.giphy.com/media/4TtvdTehawQtgOT9M3/giphy.gif)
![Alt text](https://i.imgur.com/inkRF1y.png)
![Alt text](https://i.imgur.com/1Ms8ihB.png)

## Architechture & Presentantion Pattern
- MVP

Each module is represented for a MVP (presentantion pattern) and when necessary, a layer called <b>Repository</b>, responsible to comunicate with remote and local repo's and a layer called <b>Router</b> responsible to navegate to another scene or dismiss the current one.

## Tests

The application was developed focused on testing Presenter and Repository classes which are the most critical areas in the app. The proccess is initiated by defining and describing the methods responsible to achieve the feature goals, after that the features are developed on demand in order to make the green tests.

![Alt text](https://i.imgur.com/c7NQ2YM.png)

## Third party libraries

No need of third party libraries at moment.
