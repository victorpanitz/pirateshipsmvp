//
//  ShipDetailsPresenter.swift
//  PirateShipsApp
//
//  Created by Victor Magalhaes on 18/03/19.
//  Copyright © 2019 Victor. All rights reserved.
//

import Foundation

final class ShipDetailsPresenter {
    
    private let ship: Ship
    weak var view: ShipDetailsView?
    let router: ShipDetailsRoutering
    
    init(router: ShipDetailsRoutering, ship: Ship) {
        self.ship = ship
        self.router = router
    }
    
    func attachView(_ view: ShipDetailsView) {
        self.view = view
        
        view.setTitle(ship.title ?? "Unknown")
        view.setImage(ship.imageUrl ?? "https://ichef.bbci.co.uk/images/ic/640x360/p06gsqm7.jpg")
        
        if let price = ship.price, price > 0 {
            view.setPrice(String(format: "$%.2f", price))
        } else {
            view.setPrice("$xxx.xx")
        }
        
        view.setDescription(ship.description ?? "Unknown")
    }
    
    func pirateGreetingTriggered() {
        view?.showPirateGreeting(
            ship.type == .yo ? "Yo ho hooo!" :
            ship.type == .ar ? "Arrr!" :
            ship.type == .ay ? "Aye Aye!" :
            "Ahoi!"
        )
    }
    
    func closeButtonTriggered() {
        router.dismiss()
    }
    
}
