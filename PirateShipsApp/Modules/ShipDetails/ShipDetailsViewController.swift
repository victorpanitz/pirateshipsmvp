//
//  ShipDetailsViewController.swift
//  PirateShipsApp
//
//  Created by Victor Magalhaes on 18/03/19.
//  Copyright © 2019 Victor. All rights reserved.
//

import Foundation
import UIKit

final class ShipDetailsViewController: UIViewController {

    private let imageView : UIImageView = {
        let imageV = UIImageView()
        imageV.contentMode = .scaleAspectFill
        imageV.clipsToBounds = true
        imageV.backgroundColor = UIColor.lightGray.withAlphaComponent(0.3)
        imageV.translatesAutoresizingMaskIntoConstraints = false
        
        return imageV
    }()
        
    private let titleLabel: CustomLabel = {
        let label = CustomLabel()
        label.backgroundColor = .clear
        label.font = UIFont(name: "HelveticaNeue-CondensedBlack", size: 16)
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private let descriptionTextView: UITextView = {
        let textView = UITextView()
        textView.backgroundColor = .clear
        textView.font = UIFont(name: "HelveticaNeue-Light", size: 18)
        textView.textAlignment = .justified
        textView.translatesAutoresizingMaskIntoConstraints = false
        
        return textView
    }()
    
    private let priceLabel: CustomLabel = {
        let label = CustomLabel()
        label.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        label.numberOfLines = 1
        label.font = UIFont(name: "Helvetica-Bold", size: 12)
        label.textColor = .white
        label.sizeToFit()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        
        return label
    }()
    
    private let closeButton: UIButton = {
        let button = UIButton(type: .system)
        button.tintColor = .black
        button.setImage(UIImage(named: "ic_close_selected"), for: .highlighted)
        button.setImage(UIImage(named: "ic_close_normal"), for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let greetingButton: UIButton = {
        let button = UIButton(type: .system)
        button.tintColor = .black
        button.setImage(UIImage(named: "ic_pirate_selected"), for: .highlighted)
        button.setImage(UIImage(named: "ic_pirate_normal"), for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    
    private let presenter: ShipDetailsPresenter
    
    init(presenter: ShipDetailsPresenter) {
        self.presenter = presenter
        
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) { return nil }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        setupImageView()
        setupTitleLabel()
        setupDescriptionTextView()
        setupPriceLabel()
        setupCloseButton()
        setupGreetingButton()
        
        presenter.attachView(self)
    }
    
    private func setupImageView() {
        view.addSubview(imageView)
        
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 50),
            imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            imageView.heightAnchor.constraint(equalToConstant: view.frame.height*0.3)
            ]
        )
    }
    
    private func setupTitleLabel() {
        view.addSubview(titleLabel)
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            titleLabel.heightAnchor.constraint(equalToConstant: view.frame.height*0.1)
            ]
        )
    }
    
    private func setupDescriptionTextView() {
        view.addSubview(descriptionTextView)
        
        NSLayoutConstraint.activate([
            descriptionTextView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor),
            descriptionTextView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 12),
            descriptionTextView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -12),
            descriptionTextView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ]
        )
    }
    
    private func setupPriceLabel() {
        view.addSubview(priceLabel)

        NSLayoutConstraint.activate([
            priceLabel.bottomAnchor.constraint(equalTo: imageView.bottomAnchor),
            priceLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            priceLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            priceLabel.heightAnchor.constraint(equalToConstant: view.frame.height*0.05)
            ]
        )
    }
    
    private func setupCloseButton() {
        view.addSubview(closeButton)
        
        NSLayoutConstraint.activate([
            closeButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            closeButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 18),
            closeButton.heightAnchor.constraint(equalToConstant: 20),
            closeButton.widthAnchor.constraint(equalToConstant: 20),
            closeButton.bottomAnchor.constraint(equalTo: imageView.topAnchor)
            ])
        
        closeButton.addTarget(self, action: #selector(closeButtonTouchUpInside), for: .touchUpInside)
    }
    
    private func setupGreetingButton() {
        view.addSubview(greetingButton)
        
        NSLayoutConstraint.activate([
            greetingButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            greetingButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -18),
            greetingButton.heightAnchor.constraint(equalToConstant: 29),
            greetingButton.widthAnchor.constraint(equalToConstant: 29),
            greetingButton.bottomAnchor.constraint(equalTo: imageView.topAnchor)
            ])
    
        greetingButton.addTarget(self, action: #selector(greetingButtonTouchUpInside), for: .touchUpInside)
        
    }
    
    @objc private func greetingButtonTouchUpInside() {
        presenter.pirateGreetingTriggered()
    }
    
    @objc private func closeButtonTouchUpInside() {
        presenter.closeButtonTriggered()
    }
    
}

extension ShipDetailsViewController: ShipDetailsView {
    func setTitle(_ text: String) {
        titleLabel.text = text
    }
    
    func setDescription(_ text: String) {
        descriptionTextView.text = text
    }
    
    func setImage(_ text: String) {
        imageView.loadImage(text, placeHolder: UIImage(named: "img_sea"))
    }
    
    func setPrice(_ text: String) {
        priceLabel.text = text
    }
    
    func showPirateGreeting(_ text: String) {
        let alert = UIAlertController(title: "We are pirates!", message: text, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
}
