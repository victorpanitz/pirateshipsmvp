//
//  ShipsListItemPresenter.swift
//  PirateShipsApp
//
//  Created by Victor Magalhaes on 17/03/19.
//  Copyright © 2019 Victor. All rights reserved.
//

import Foundation

final class ShipsListItemPresenter {
    
    private let image: String
    private let title: String
    private let price: Double
    
    private weak var view: ShipsListItemView?
    
    init(title: String?, image: String?, price: Double?) {
        self.title = title ?? "Unknown"
        self.image = image ?? "https://ichef.bbci.co.uk/images/ic/640x360/p06gsqm7.jpg"
        self.price = price ?? 0
    }
    
    func attachView(_ view: ShipsListItemView) {
        self.view = view
        
        view.setImage(image)
        view.setTitle(title)
        
        price > 0
            ? view.setPrice(String(format: "$%.2f", price))
            : view.setPrice("$xxx.xx")
        
    }
    
    func itemTouchBegan() {
        view?.animateAsResponse()
    }
    
    func itemTouchEnded() {
        view?.animateToIdentity()
    }
    
    func itemTouchCancelled() {
        view?.animateToIdentity()
    }
    
}
