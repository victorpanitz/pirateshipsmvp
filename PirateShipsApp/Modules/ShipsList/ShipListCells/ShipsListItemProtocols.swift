//
//  ShipsListItemProtocols.swift
//  PirateShipsApp
//
//  Created by Victor Magalhaes on 17/03/19.
//  Copyright © 2019 Victor. All rights reserved.
//

protocol ShipsListItemView: AnyObject {
    func setImage(_ text: String)
    func setTitle(_ text: String)
    func setPrice(_ text: String)
    func animateAsResponse()
    func animateToIdentity()
}

