//
//  ShipsListRouter.swift
//  PirateShipsApp
//
//  Created by Victor Magalhaes on 17/03/19.
//  Copyright © 2019 Victor. All rights reserved.
//

import Foundation
import UIKit

class ShipsListRouter: ShipsListRoutering {
    
    private weak var view: UIViewController?

    init() {}
    
    func makeViewController() -> UIViewController {
        let repository = ShipsListRepository()
        let presenter = ShipsListPresenter(router: self, repository: repository)
        let viewController = ShipsListViewController(presenter: presenter)
        self.view = viewController
        
        return viewController
    }
    
    func navigateToShipDetails(ship: Ship) {
        let router = ShipDetailsRouter(ship: ship)
        view?.present(router.makeViewController(), animated: true, completion: nil)
    }
}
