//
//  ShipsListPresenter.swift
//  PirateShipsApp
//
//  Created by Victor Magalhaes on 13/03/19.
//  Copyright © 2019 Victor. All rights reserved.
//

final class ShipsListPresenter {
    
    private var ships: [Ship]?
    private weak var view: ShipsListView?
    private let router: ShipsListRoutering
    private let repository: ShipsListRepositoryInput
    
    init(router: ShipsListRoutering, repository: ShipsListRepositoryInput) {
        self.router = router
        self.repository = repository
        
        repository.output = self
    }
    
    func attatchView(_ view: ShipsListView) {
        self.view = view

        repository.retrieveShips()
        
        view.showLoading()
        repository.fetchPirateShips()
    }
    
    func refreshControlTriggered() {
        view?.showLoading()
        repository.fetchPirateShips()
    }
    
    func cellGotTriggered(index: Int) {
        guard
            let ships = ships,
            (0..<ships.count).contains(index)
            else { return }
        
        router.navigateToShipDetails(ship: ships[index])
    }
    
}

extension ShipsListPresenter: ShipsListRepositoryOutput {
    func updateShipsListSucceeded() {
        repository.retrieveShips()
    }
    
    func updateShipsListFailed(error: String) {
        view?.showError(message: error)
    }
    
    func fetchDataSucceeded(ships: [Ship]) {
        view?.hideLoading()
        repository.updateShipsList(ships)
    }
    
    func fetchDataFailed(error: String) {
        view?.hideLoading()
        view?.showError(message: error)
    }
    
    func retrieveLocalDataSucceeded(ships: [Ship]) {
        let shipsSorted = ships.sorted(by: {$0.title ?? "" > $1.title ?? ""})
        self.ships = shipsSorted
        
        /*
         great point to discuss with ux team
         to show a friendly empty screen
         when ships.count == 0  :)
         */
        
        view?.updateShipsList(shipsSorted)
        
    }
    
    func retrieveLocalDataFailed(error: String) {
        view?.showError(message: error)
    }
    
}
