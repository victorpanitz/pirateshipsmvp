//
//  ShipOutput.swift
//  PirateShipsApp
//
//  Created by Victor Magalhaes on 14/03/19.
//  Copyright © 2019 Victor. All rights reserved.
//

import Foundation

struct ShipOutput: Decodable {
    var id: Int
    var title: String?
    var image: String?
    var detail: String?
    var type: String?
    var price: Double?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "title"
        case detail = "description"
        case price = "price"
        case image = "image"
        case type = "greeting_type"
    }
}
